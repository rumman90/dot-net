﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagmentSystem.Models
{
    public class Designation
    {
        public int Id { set; get; }
        public string Designations { get; set; }
    }
}