﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace UniversityManagmentSystem.Models
{
    public class DBGetway
    {
        //private SqlConnection connectionObj;
        //public SqlConnection ConnectionObj
        //{
        //    get
        //    {
        //        return connectionObj;
        //    }
        //}
        //private SqlCommand commandObj;
        //public SqlCommand CommandObj
        //{
        //    get
        //    {
        //        commandObj.Connection = connectionObj;
        //        return commandObj;
        //    }
        //}
        //public DBGetway()
        //{
        //    connectionObj = new SqlConnection(WebConfigurationManager.ConnectionStrings["VoidUniversityDB"].ConnectionString);
        //    commandObj=new SqlCommand();
        //}
        public SqlCommand Command { get; set; }
        public SqlDataReader DataReader { get; set; }
        public SqlDataAdapter DataAdapter { get; set; }
        public string Query { get; set; }
        public static SqlConnection ConnectGlobal()
        {
            string connectionString = WebConfigurationManager.ConnectionStrings["UniversityDB"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }
    }
}