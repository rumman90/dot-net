﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UniversityManagmentSystem.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string RegNo { get; set; }
        [Required(ErrorMessage = "Name is required!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email is required!")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Contact is required!")]
        [RegularExpression("[^0-9]", ErrorMessage = "Contact Must be Numeric")]
        [DisplayName("Contact No.")]
        public string Contact { get; set; }

        //[Required(ErrorMessage = "Date is required!")]
        //[DisplayName("Date")]
        //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Date")]
        public DateTime RegDate { get; set; }

        [Required(ErrorMessage = "Address is required!")]
        public string Address { get; set; }
        [DisplayName("Department")]
        public int DepartmentId { get; set; }
    }
}