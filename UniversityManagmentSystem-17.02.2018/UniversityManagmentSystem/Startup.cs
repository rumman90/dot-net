﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UniversityManagmentSystem.Startup))]
namespace UniversityManagmentSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
