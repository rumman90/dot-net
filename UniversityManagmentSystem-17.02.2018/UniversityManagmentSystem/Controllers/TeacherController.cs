﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagmentSystem.Manager;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Controllers
{
    public class TeacherController : Controller
    {
        private string _message;
        TeacherManager _teacherManager = new TeacherManager();
        DesignationManager _designationManager = new DesignationManager();
        DepartmentManager _departmentManager = new DepartmentManager();
        //
        // GET: /Teacher/
        public ActionResult Index()
        {
            List<Teacher> teachers = _teacherManager.GetAll().ToList();
            return View(teachers);
        }

        //
        // GET: /Teacher/Create
        public ActionResult TeacherCreate()
        {
            IEnumerable<dynamic> desinationList = _designationManager.GetAll;
            IEnumerable<Department> departments = _departmentManager.GetAll();
            ViewBag.Designations = desinationList;
            ViewBag.Departments = departments;
            return View();
        }

        //
        // POST: /Teacher/Create
        [HttpPost]
        public ActionResult TeacherCreate(Teacher teacher)
        {
            try
            {
                _message = _teacherManager.Save(teacher);
                IEnumerable<Designation> desinationList = _designationManager.GetAll;
                IEnumerable<Department> departments = _departmentManager.GetAll();
                ViewBag.Designations = desinationList;
                ViewBag.Departments = departments;
                ViewBag.Message = _message;
                return View();
                //return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                _message = exception.Message;
                if (exception.InnerException != null)
                {
                    _message += "<br/>System Error:" + exception.InnerException.Message;

                }
                ViewBag.Message = _message;
                return View();
            }
        }
	}
}