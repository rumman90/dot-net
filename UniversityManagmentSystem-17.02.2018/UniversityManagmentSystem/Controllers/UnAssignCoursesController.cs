﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagmentSystem.Manager;

namespace UniversityManagmentSystem.Controllers
{
    public class UnAssignCoursesController : Controller
    {
        CourseManager courseManager = new CourseManager();
        //
        // GET: /UnAssignCourses/
        public ActionResult UnAssignCourses()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UnAssignCourses(int? id)
        {
            ViewBag.Message = courseManager.UnAssignCourses();
            return View();
        }
	}
}