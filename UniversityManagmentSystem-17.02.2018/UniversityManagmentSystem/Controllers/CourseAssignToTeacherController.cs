﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagmentSystem.Manager;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Controllers
{
    public class CourseAssignToTeacherController : Controller
    {
        CourseManager _courseManger = new CourseManager();
        DepartmentManager _departmentManager = new DepartmentManager();
        TeacherManager _teacherManager = new TeacherManager();
        CourseAssignToTeacherManager courseAssignToTeacherManager = new CourseAssignToTeacherManager();
        //
        // GET: /CourseAssignToTeacher/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AssignCourseToTeacher()
        {
            IEnumerable<Course> courses = _courseManger.GetAll;
            IEnumerable<Department> departments = _departmentManager.GetAll();
            IEnumerable<Teacher> teachers = _teacherManager.GetAll();
            ViewBag.Courses = courses;
            ViewBag.Departments = departments;
            ViewBag.Teachers = teachers;
            return View();
        }
        [HttpPost]
        public ActionResult AssignCourseToTeacher(CourseAssignToTeacher courseAssign)
        {
            try
            {
                ViewBag.Message = courseAssignToTeacherManager.Save(courseAssign);
                IEnumerable<Course> courses = _courseManger.GetAll;
                IEnumerable<Department> departments = _departmentManager.GetAll();
                IEnumerable<Teacher> teachers = _teacherManager.GetAll();
                ViewBag.Courses = courses;
                ViewBag.Departments = departments;
                ViewBag.Teachers = teachers;
                return View();
            }
            catch (Exception exception)
            {

                ViewBag.Message = exception.InnerException.Message;
                return View();
            }
        }
        public JsonResult GetCourseInformationByDepartmentId(int departmentId)
        {
            IEnumerable<CourseViewModel> courseViewModels = _courseManger.GetCourseViewModels.ToList().FindAll(deptId => deptId.DepartmentId == departmentId);
            return Json(courseViewModels, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTeachersByDepartmentId(int departmentId)
        {
            IEnumerable<Teacher> teachers = _teacherManager.GetAll();
            var teacherList = teachers.ToList().FindAll(t => t.DepartmentId == departmentId);
            return Json(teacherList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCoursesByDepartmentId(int departmentId)
        {
            IEnumerable<Course> courses = _courseManger.GetAll;
            var courseList = courses.ToList().FindAll(c => c.DepartmentId == departmentId);
            return Json(courseList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTeacherById(int teacherId)
        {
            IEnumerable<Teacher> teachers = _teacherManager.GetAll();
            Teacher aTeacher = teachers.ToList().Find(t => t.Id == teacherId);
            return Json(aTeacher, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCourseById(int courseId)
        {
            IEnumerable<Course> courses = _courseManger.GetAll;
            Course aCourse = courses.ToList().Find(c => c.Id == courseId);
            return Json(aCourse, JsonRequestBehavior.AllowGet);
        }
	}
}