﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagmentSystem.Manager;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Controllers
{
    public class CourseController : Controller
    {
        DepartmentManager departmentManager = new DepartmentManager();
        SemesterManager semesterManager = new SemesterManager();
        CourseManager courseManager = new CourseManager();
        //
        // GET: /Course/
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult CourseSave()
        {
            List<Department> departments = departmentManager.GetAll().ToList();
            List<Semester> semesters = semesterManager.GetAll.ToList();
            ViewBag.Departments = departments;
            ViewBag.Semesters = semesters;
            return View();
        }


        // POST: /Course/Create
        [HttpPost]
        public ActionResult CourseSave(Course aCourse)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string message = courseManager.Save(aCourse);
                    ViewBag.Message = message;

                    List<Department> departments = departmentManager.GetAll().ToList();
                    List<Semester> semesters = semesterManager.GetAll.ToList();
                    ViewBag.Departments = departments;
                    ViewBag.Semesters = semesters;
                    return View();

                    //return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ViewBag.message = exception.InnerException.Message;
                    return View();
                }
            }
            return View();
        }
    }
}