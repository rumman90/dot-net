﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagmentSystem.Manager;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Controllers
{
    public class StudentResultController : Controller
    {
        StudentManager _studentManager = new StudentManager();
        CourseManager _courseManager = new CourseManager();

        // GET: /StudentResult/Create
        public ActionResult Save()
        {
            IEnumerable<Student> students = _studentManager.GetAll;
            IEnumerable<Course> courses = _courseManager.GetAll;
            ViewBag.Students = students;
            ViewBag.Courses = courses;
            return View();
        }

        //
        // POST: /StudentResult/Create
        [HttpPost]
        public ActionResult Save(StudentResult studentResult)
        {
            try
            {
                ViewBag.Message = _studentManager.Save(studentResult);
                IEnumerable<Student> students = _studentManager.GetAll;
                IEnumerable<Course> courses = _courseManager.GetAll;
                ViewBag.Students = students;
                ViewBag.Courses = courses;

                return View();
            }
            catch (Exception exception)
            {
                ViewBag.Message = exception.InnerException.Message;
                return View();
            }
        }


        public JsonResult GetStudentById(int studentId)
        {
            StudentViewModel student = _studentManager.GetStudentInformationById(studentId);
            return Json(student, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCoursesTakebByStudent(int studentId)
        {
            IEnumerable<Course> courses = _courseManager.GetCoursesTakenByaStudentById(studentId);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult ViewStudentResult()
        {
            IEnumerable<Student> students = _studentManager.GetAll;
            ViewBag.Students = students;
            return View();
        }

        public JsonResult GetStudentResultByStudentId(int studentId)
        {
            IEnumerable<StudentResultViewModel> studentResults = _studentManager.GetStudentResultByStudentId(studentId);
            return Json(studentResults, JsonRequestBehavior.AllowGet);
        }


    }
}