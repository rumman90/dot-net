﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using UniversityManagmentSystem.Manager;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Controllers
{
    public class DepartmentController : Controller
    {
        DepartmentManager _departmentManager=new DepartmentManager();
        //
        // GET: /Department/
        public ActionResult Index()
        {
            List<Department> departments = _departmentManager.GetAll().ToList();
            return View(departments);
            //return View();
        }

        public ActionResult DepartmentCreate()
        {
            return View();
        }
        [HttpPost]
        public ActionResult DepartmentCreate(Department departments)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    string mess = _departmentManager.Save(departments);
                    ViewBag.message = mess;
                    return View();
                    //return RedirectToAction("Index");
                }
                catch (Exception exception)
                {
                    ViewBag.message = exception.InnerException.Message; 
                    return View();
                }
            }
            return View();
        }
	}
}