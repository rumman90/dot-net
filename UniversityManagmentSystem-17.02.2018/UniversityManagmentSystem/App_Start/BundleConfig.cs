﻿using System.Web;
using System.Web.Optimization;

namespace UniversityManagmentSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/ContentHome").Include(
            "~/ContentHome/plugins/jQuery/jquery-2.2.3.min.js",
            "~/ContentHome/bootstrap/js/bootstrap.min.js",
            "~/ContentHome/plugins/select2/select2.full.min.js",

            "~/ContentHome/plugins/input-mask/jquery.inputmask.js",
            "~/ContentHome/plugins/input-mask/jquery.inputmask.date.extensions.js",
            "~/ContentHome/plugins/input-mask/jquery.inputmask.extensions.js",

            //"~/ContentHome/plugins/daterangepicker/daterangepicker.js",
            "~/ContentHome/plugins/datepicker/bootstrap-datepicker.js",
            //"~/ContentHome/plugins/colorpicker/bootstrap-colorpicker.min.js",
            "~/ContentHome/plugins/timepicker/bootstrap-timepicker.min.js",

            "~/ContentHome/plugins/slimScroll/jquery.slimscroll.min.js",
            "~/ContentHome/plugins/iCheck/icheck.min.js",
            "~/ContentHome/plugins/fastclick/fastclick.js",

             "~/ContentHome/dist/js/app.min.js",
             "~/ContentHome/dist/js/demo.js",
              "~/ContentHome/documentation/docs.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/ContentHome/Account").Include(
            "~/ContentHome/Account/js/jquery.min.js",
            "~/ContentHome/Account/bootstrap/js/bootstrap.min.js",
             "~/ContentHome/Account/js/my-login.js"
            ));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));
            bundles.Add(new StyleBundle("~/ContentHome/css").Include(
            "~/ContentHome/bootstrap/css/bootstrap.min.css",
            //"~/ContentHome/plugins/iCheck/all.css",
            //"~/ContentHome/plugins/iCheck/flat/blue.css",
            //"~/ContentHome/plugins/jvectormap/jquery-jvectormap-1.2.2.css",

            "~/ContentHome/plugins/datepicker/datepicker3.css",
            //"~/ContentHome/plugins/iCheck/all.css",
            //"~/ContentHome/plugins/colorpicker/bootstrap-colorpicker.min.css",
            "~/ContentHome/plugins/timepicker/bootstrap-timepicker.min.css",
            "~/ContentHome/plugins/select2/select2.min.css",
            "~/ContentHome/dist/css/Admin.min.css",
            "~/ContentHome/dist/css/skins/_all-skins.min.css",
            "~/ContentHome/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"
            ));

            bundles.Add(new StyleBundle("~/ContentHome/Account/css").Include(
          "~/ContentHome/Account/bootstrap/css/bootstrap.min.css",
          "~/Content/site.css",
          "~/ContentHome/Account/css/my-login.css"));
        }
    }
}
