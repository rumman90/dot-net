﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Getway;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Manager
{
    public class DayManager
    {
        DayGateway dayGateway = new DayGateway();
        public IEnumerable<Day> GetAllDays
        {
            get { return dayGateway.GetAllDays; }
        } 
    }
}