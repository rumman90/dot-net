﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Getway;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Manager
{
    public class TeacherManager
    {
        TeacherGateway teacherGateway = new TeacherGateway();

/*Teache Information Save Start*/
        public string Save(Teacher teacher)
        {
            if (!(IsEmailAddressValid(teacher.Email)))
            {
                return "Please! Enter a valid Email Address";
            }
            if (IsEmailAddressExits(teacher.Email))
            {
                return "Email Address must be unique";
            }
            if (teacherGateway.Insert(teacher) > 0)
            {
                return "Teacher Information Saved Sucessfully";
            }
            return "Teacher Information Save Faild.";
        }
/*Teache Information Save End*/

/*Teache Information Validation Start*/
        private bool IsEmailAddressExits(string email)
        {
            Teacher aTeacher = teacherGateway.GetTeacherByEmailAddress(email);
            if (aTeacher != null)
            {
                return true;
            }
            return false;
        }

        private bool IsEmailAddressValid(string email)
        {
            if (email.Contains(".com") && ((email.Contains("@gmail")) || (email.Contains("@yahoo"))))
            {
                return true;
            }
            return false;
        }
/*Teache Information Validation End*/

/*Get Teache Information Start*/
        public IEnumerable<Teacher> GetAll()
        {
            return teacherGateway.GetAll();
        }
/*Get Teache Information End*/
    }
}