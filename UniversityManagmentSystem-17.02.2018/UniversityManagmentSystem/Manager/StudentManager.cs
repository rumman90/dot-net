﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Getway;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Manager
{
    public class StudentManager
    {
        StudentGateway _studentGateway = new StudentGateway();
        DepartmentGetway _departmentGateway = new DepartmentGetway();
        public IEnumerable<Student> GetAll
        {
            get { return _studentGateway.GetAll; }
        }

        public string GetLastAddedStudentRegistration(string searchKey)
        {
            return _studentGateway.GetLastAddedStudentRegistration(searchKey);
        }

        public string Save(Student aStudent)
        {
            int counter;
            Department department = _departmentGateway.GetAll().Single(depid => depid.Id == aStudent.DepartmentId);
            string searchKey = department.Code + "-" + aStudent.RegDate.Year + "-";
            string lastAddedRegistrationNo = GetLastAddedStudentRegistration(searchKey);
            if (lastAddedRegistrationNo == null)
            {
                aStudent.RegNo = searchKey + "001";

            }

            if (lastAddedRegistrationNo != null)
            {
                string tempId = lastAddedRegistrationNo.Substring((lastAddedRegistrationNo.Length - 3), 3);
                counter = Convert.ToInt32(tempId);
                string studentSl = (counter + 1).ToString();

                if (studentSl.Length == 1)
                {
                    aStudent.RegNo = searchKey + "00" + studentSl;
                }
                else if (studentSl.Count() == 2)
                {
                    aStudent.RegNo = searchKey + "0" + studentSl;
                }
                else
                {
                    aStudent.RegNo = searchKey + studentSl;
                }
            }
            var listOfEmailAddress = from student in GetAll
                                     select student.Email;

            string tempEmail = listOfEmailAddress.ToList().Find(email => email.Contains(aStudent.Email));

            if (tempEmail != null)
            {
                return "Email address must be unique";
            }
            if (IsEmailAddressValid(aStudent.Email))
            {
                if (_studentGateway.Insert(aStudent) > 0)
                {
                    return "Student Registration Successfully!; Registration No: " + aStudent.RegNo + "; Name: " + aStudent.Name + "; Email: " + aStudent.Email + "; Contact Number: " + aStudent.Contact;
                }

                return "Student Registration Failed";
            }
            return "Please! enter a valid email address";
        }
        private bool IsEmailAddressValid(string email)
        {
            if (email.Contains(".com") && ((email.Contains("@gmail")) || (email.Contains("@yahoo")) || (email.Contains("@live")) || (email.Contains("@outlook"))))
            {
                return true;
            }
            return false;
        }

        public StudentViewModel GetStudentInformationById(int id)
        {
            return _studentGateway.GetStudentInformationById(id);
        }

        /* Course Enroll Save*/
        public string Save(EnrollStudentInCourse enrollStudentInCourse)
        {
            EnrollStudentInCourse enrollStudent =
                GetEnrollCourses.ToList()
                    .Find(
                        st =>
                            (st.StudentId == enrollStudentInCourse.StudentId &&
                            st.CourseId == enrollStudentInCourse.CourseId) && (st.Status));
            if (enrollStudent == null)
            {
                if (_studentGateway.Insert(enrollStudentInCourse) > 0)
                {
                    return "Course Enroll Successfully!";
                }
                return "Course Enroll Failed";
            }

            return "This course already taken by the student";
        }

        public IEnumerable<EnrollStudentInCourse> GetEnrollCourses
        {
            get { return _studentGateway.GetEnrollCourses; }
        }
        /* Student Result Save*/
        public string Save(StudentResult studentResult)
        {
            StudentResult result =
                GetAllResult.ToList()
                    .Find(st => st.StudentId == studentResult.StudentId && st.CourseId == studentResult.CourseId);
            if (result == null)
            {
                if (_studentGateway.Insert(studentResult) > 0)
                {
                    return "Student Result Saved sucessfull!";
                }
                return "Student Result Save Failed";
            }

            if (result.Status)
            {
                if (_studentGateway.UpdateStudentResult(studentResult) > 0)
                {
                    return "Student Result Update Sucessfull!";
                }
                return "Student Result Update Faild";
            }
            return "This course result already saved";
        }

        public IEnumerable<StudentResult> GetAllResult
        {
            get { return _studentGateway.GetAllResult; }
        }

        public IEnumerable<StudentResultViewModel> GetStudentResultByStudentId(int id)
        {
            return _studentGateway.GetStudentResultByStudentId(id);
        }


    }
}