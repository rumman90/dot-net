﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Getway;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Manager
{
    public class CourseManager
    {
        CourseGateway courseGateway = new CourseGateway();
/*Course Save Start*/
        public string Save(Course aCourse)
        {
            if (!(IsCorseCodeValid(aCourse)))
            {
                return "Course code must be at least 5 character of length";
            }
            if (IsCourseCodeExits(aCourse.Code))
            {
                return "Course code Already Exists ! Code must be unique";
            }
            if (IsCourseNameExits(aCourse.Name))
            {
                return "Course Name Already Exists ! Name must be unique";
            }
            if (courseGateway.Insert(aCourse) > 0)
            {
                return "Course Information Saved Successfully";
            }
            return "Course Information Saved Failed";
        }
/*Course Save End*/

/*Course Validation Start*/
        private bool IsCourseNameExits(string name)
        {

            Course course = courseGateway.GetCourseByName(name);
            if (course != null)
            {
                return true;
            }

            return false;
        }

        private bool IsCourseCodeExits(string code)
        {
            Course course = courseGateway.GetCourseByCode(code.ToUpper());

            if (course != null)
            {
                return true;
            }

            return false;
        }

        private bool IsCorseCodeValid(Course aCourse)
        {
            if (aCourse.Code.Length > 5)
            {
                return true;
            }
            return false;

        }
/*Course Validation End*/
/*Get Course Information Start*/
        public IEnumerable<Course> GetAll
        {
            get { return courseGateway.GetAll; }
        }
        public IEnumerable<CourseViewModel> GetCourseViewModels
        {
            get { return courseGateway.GetCourseViewModels; }
        }

        public IEnumerable<Course> GetCoursesTakenByaStudentById(int id)
        {
            return courseGateway.GetCoursesTakeByaStudentByStudentId(id);
        }
        public IEnumerable<Course> GetCourseByDepartmentId(int departmentId)
        {
            return courseGateway.GetCourseByDepartmentId(departmentId);
        }
        /*Get Course Information End*/
        /*Unassign Course Information Start*/
        public string UnAssignCourses()
        {
            if (courseGateway.UnAssignCourse() > 0)
            {
                return "Courses Unassign Successfully!";
            }
            return "Courses Unassign Failed";
        }
        /*Unassign Course Information End*/

    }
}