﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Getway;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Manager
{
    public class DesignationManager
    {
        DesignationGateway _designationGateway = new DesignationGateway();
        public IEnumerable<Designation> GetAll
        {

            get { return _designationGateway.GetAll; }
        } 
    }
}