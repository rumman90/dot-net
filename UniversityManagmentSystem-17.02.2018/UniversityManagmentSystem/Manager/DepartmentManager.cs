﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Getway;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Manager
{
    public class DepartmentManager
    {
        DepartmentGetway _departmentGetway = new DepartmentGetway();

        public string Save(Department aDepartment)
        {
            if (!(IsDepartmentCodeValid(aDepartment)))
            {
                return "Department code must be Between 2 to 7 Character Length";
            }
            if (IsDepartmentCodeExits(aDepartment))
            {
                return "Department Code Already Exists.";
            }
            if (IsDepartmentNameExits(aDepartment))
            {
                return "Department Name Already Exists.";
            }
            if (_departmentGetway.Insert(aDepartment) > 0)
            {
                return "Department Saved Successfully";
            }

            return "Department Save Faild";
        }

        private bool IsDepartmentNameExits(Department aDepartment)
        {
            Department dept = _departmentGetway.GetDepartmentByName(aDepartment.Name);

            if (dept != null)
            {
                return true;
            }

            return false;
        }

        private bool IsDepartmentCodeExits(Department aDepartment)
        {
            Department dept = _departmentGetway.GetDepartmentByCode(aDepartment.Code.ToUpper());

            if (dept != null)
            {
                return true;
            }

            return false;
        }

        private bool IsDepartmentCodeValid(Department aDepartment)
        {
            if (aDepartment.Code.Length >= 2 || aDepartment.Code.Length <= 7)
            {
                return true;
            }

            return false;
        }
        public IEnumerable<Department> GetAll()
        {
            return _departmentGetway.GetAll();
        }
    }
}