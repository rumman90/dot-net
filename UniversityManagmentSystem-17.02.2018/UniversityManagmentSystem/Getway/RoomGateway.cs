﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class RoomGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
        public IEnumerable<Room> GetAllRooms
        {
            get
            {
                try
                {
                    var roomList = new List<Room>();
                    Query = "SELECT * FROM t_Room";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    SqlDataReader reader = Command.ExecuteReader();

                    while (reader.Read())
                    {
                        var room = new Room
                        {
                            Id = Convert.ToInt32(reader["Id"].ToString()),
                            Name = reader["Name"].ToString()
                        };
                        roomList.Add(room);
                    }
                    reader.Close();
                    return roomList;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect Rooms", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }
            }
        }
    }
}