﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class ClassRoomGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
        public int Insert(ClassRoom room)
        {
            try
            {
                Query = "INSERT INTO t_AllocateClassRoom(DepartmentId,CourseId,RoomId,DayId,StartTime,EndTime,AllocationStatus) VALUES(@deptId,@courseId,@roomId,@dayId,@StartTime,@endTime,@allocationStatus)";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@deptId", room.DepartmentId);
                Command.Parameters.AddWithValue("@courseId", room.CourseId);
                Command.Parameters.AddWithValue("@roomId", room.RoomId);
                Command.Parameters.AddWithValue("@dayId", room.DayId);
                Command.Parameters.AddWithValue("@startTime", room.StartTime.ToShortTimeString());
                Command.Parameters.AddWithValue("@endTime", room.Endtime.ToShortTimeString());
                Command.Parameters.AddWithValue("@allocationStatus", 1);
                Connection.Open();
                int rowAffected = Command.ExecuteNonQuery();
                return rowAffected;
            }
            catch (Exception exception)
            {
                throw new Exception("Could not Saved, Allocate Class Room", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }

        public List<ClassSchedule> GetAll
        {
            get
            {
                try
                {
                    Query= "SELECT * FROM classSchedule";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<ClassSchedule> scheduleList = new List<ClassSchedule>();
                    while (DataReader.Read())
                    {
                        ClassSchedule schedule = new ClassSchedule
                        {
                            CourseId = Convert.ToInt32(DataReader["CourseId"].ToString()),
                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                            CourseCode = DataReader["Code"].ToString(),
                            CourseName = DataReader["Name"].ToString(),
                            Schedule = DataReader["Schedule_Inforamtion"].ToString()
                        };
                        scheduleList.Add(schedule);
                    }
                    DataReader.Close();
                    return scheduleList;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect class schedule", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }

            }
        }

        public List<TempClassSchedule> GetAllClassSchedules
        {
            get
            {
                try
                {
                   
                    Query = "SELECT * FROM ScheduleOfClass";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<TempClassSchedule> scheduleList = new List<TempClassSchedule>();
                    while (DataReader.Read())
                    {
                        TempClassSchedule schedule = new TempClassSchedule
                        {

                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                            CourseCode = DataReader["Code"].ToString(),
                            CourseName = DataReader["Name"].ToString(),
                            RoomName = DataReader["Room_Name"].ToString(),
                            DayName = DataReader["Day_Name"].ToString(),
                            StartTime = Convert.ToDateTime(DataReader["StartTime"].ToString()),
                            EndTime = Convert.ToDateTime(DataReader["EndTime"].ToString())
                        };
                        scheduleList.Add(schedule);
                    }
                    DataReader.Close();
                    return scheduleList;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect class schedule", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }
            }
        }

        public List<TempClassSchedule> GetClassSchedulByStartAndEngingTime(DateTime startTime, DateTime endTime)
        {
            Command.CommandText = " Select * from ScheduleOfClass where  StartTime BETWEEN CAST('" + startTime + "' As Time) AND CAST('" + endTime + "' As Time)";
            return null;

        }

        public List<ClassRoom> GetClassSchedulByStartAndEndingTime(int roomId, int dayId, DateTime startTime, DateTime endTime)
        {
            try
            {               
                Query = "Select * from t_AllocateClassRoom Where DayId=" + dayId + " AND RoomId=" + roomId + " AND AllocationStatus=" + 1;
                Command = new SqlCommand(Query, Connection);
                Connection.Open();
                DataReader = Command.ExecuteReader();
                List<ClassRoom> tempClassSchedules = new List<ClassRoom>();
                while (DataReader.Read())
                {
                    ClassRoom temp = new ClassRoom
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        CourseId = Convert.ToInt32(DataReader["CourseId"].ToString()),
                        RoomId = Convert.ToInt32(DataReader["RoomId"].ToString()),
                        DayId = Convert.ToInt32(DataReader["DayId"].ToString()),
                        StartTime = Convert.ToDateTime(DataReader["StartTime"].ToString()),
                        Endtime = Convert.ToDateTime(DataReader["EndTime"].ToString())

                    };
                    tempClassSchedules.Add(temp);
                }
                DataReader.Close();
                return tempClassSchedules;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to collect class schedule", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }

        }

        public IEnumerable<TempClassSchedule> GetAllClassSchedulesByDeparmentId(int departmentId, int courseId)
        {
            try
            {
                
                Query = "SELECT * FROM ScheduleOfClass WHERE DepartmentId='" + departmentId + "' AND CourseId='" + courseId + "' AND AllocationStatus='" + 1 + "'";
                Command = new SqlCommand(Query, Connection);
                Connection.Open();
                DataReader = Command.ExecuteReader();
                List<TempClassSchedule> scheduleList = new List<TempClassSchedule>();
                while (DataReader.Read())
                {
                    TempClassSchedule schedule = new TempClassSchedule
                    {

                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        CourseCode = DataReader["Code"].ToString(),
                        CourseName = DataReader["Name"].ToString(),
                        RoomName = DataReader["Room_Name"].ToString(),
                        DayName = DataReader["Day_Name"].ToString(),
                        StartTime = Convert.ToDateTime(DataReader["StartTime"].ToString()),
                        EndTime = Convert.ToDateTime(DataReader["EndTime"].ToString()),
                        Status = Convert.ToBoolean(DataReader["AllocationStatus"])
                    };

                    scheduleList.Add(schedule);
                }

                DataReader.Close();
                //Connection.Close();
                return scheduleList;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to collect class schedule", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }

        }

        public List<ClassRoom> GetAllClassroomInformation
        {
            get
            {
                try
                {

                    Query = "SELECT * FROM t_AllocateClassRoom";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<ClassRoom> scheduleList = new List<ClassRoom>();
                    while (DataReader.Read())
                    {
                        ClassRoom classRoom = new ClassRoom
                        {
                            Id = Convert.ToInt32(DataReader["Id"].ToString()),
                            CourseId = Convert.ToInt32(DataReader["CourseId"].ToString()),
                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                            DayId = Convert.ToInt32(DataReader["DayId"].ToString()),
                            RoomId = Convert.ToInt32(DataReader["RoomId"].ToString()),
                            StartTime = Convert.ToDateTime(DataReader["StartTime"].ToString()),
                            Endtime = Convert.ToDateTime(DataReader["EndTime"].ToString()),

                            AlloctionStaus = Convert.ToBoolean(DataReader["AllocationStatus"].ToString())
                        };
                        scheduleList.Add(classRoom);
                    }
                    DataReader.Close();
                    return scheduleList;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect class schedule", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }

            }
        }

        public int UnAllocateClassRoom()
        {
            Connection.Open();
            SqlTransaction sqlTransaction = Connection.BeginTransaction();
            try
            {
                Query = "UPDATE t_AllocateClassRoom SET AllocationStatus=0";
                Command = new SqlCommand(Query, Connection);
                Command.Transaction = sqlTransaction;
                int i = Command.ExecuteNonQuery();

                sqlTransaction.Commit();
                return i;
            }
            catch (Exception exception)
            {
                sqlTransaction.Rollback();
                throw new Exception("Failed to UnAllocate Class Room", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }
    }
}