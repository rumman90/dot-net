﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.Owin.BuilderProperties;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class StudentGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
/* Get Student Infromation All*/
        public IEnumerable<Student> GetAll
        {
            get
            {
                try
                {
                    Query = "SELECT * FROM t_Student";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<Student> students = new List<Student>();
                    while (DataReader.Read())
                    {
                        Student student = new Student
                        {
                            Id = Convert.ToInt32(DataReader["Id"].ToString()),
                            RegNo = DataReader["RegNo"].ToString(),
                            Name = DataReader["Name"].ToString(),
                            Email = DataReader["Email"].ToString(),
                            Address = DataReader["Address"].ToString(),
                            Contact = DataReader["ContactNo"].ToString(),
                            RegDate = Convert.ToDateTime(DataReader["RegisterationDate"].ToString()),
                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString())
                        };
                        students.Add(student);
                    }
                    DataReader.Close();
                    return students;
                }
                catch (Exception exception)
                {
                    throw new Exception("Could not collect Studens", exception);
                }
                finally
                {
                    Command.Dispose();
                    Connection.Close();
                }
            }
        }
        /* Get Student Last Registration Number By Search Key*/
        public string GetLastAddedStudentRegistration(string searchKey)
        {
            Query = "SELECT * FROM t_Student WHERE RegNo LIKE '%" + searchKey + "%' and Id=(SELECT Max(Id) FROM t_Student WHERE RegNo LIKE '%" + searchKey + "%' )";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Student aStudent = null;
            string regNo = null;
            DataReader = Command.ExecuteReader();
            if (DataReader.Read())
            {
                aStudent = new Student
                {
                    Id = Convert.ToInt32(DataReader["Id"].ToString()),
                    Name = DataReader["Name"].ToString(),
                    RegNo = DataReader["RegNo"].ToString(),
                    Email = DataReader["Email"].ToString(),
                    Contact = DataReader["ContactNo"].ToString(),

                };
                regNo = aStudent.RegNo;
            }

            Connection.Close();
            Command.Dispose();
            DataReader.Close();
            return regNo;
        }
        /*Get Student Information By Student Id Using Stored Procedure*/
        public StudentViewModel GetStudentInformationById(int id)
        {

            try
            {
                Command = new SqlCommand("Pro_GetStudentInformationById", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Id", id);
                StudentViewModel student = null;
                Connection.Open();
                DataReader = Command.ExecuteReader();
                if (DataReader.Read())
                {
                    student = new StudentViewModel
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        RegNo = DataReader["RegNo"].ToString(),
                        Name = DataReader["Name"].ToString(),
                        Email = DataReader["Email"].ToString(),
                        ContactNo = DataReader["ContactNo"].ToString(),
                        RegisterationDate = Convert.ToDateTime(DataReader["RegisterationDate"].ToString()),
                        Address = DataReader["Address"].ToString(),
                        Department = DataReader["Department"].ToString()
                    };
                }

                DataReader.Close();
                return student;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to get student information", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }

        /*Get Student Course Enroll Infromation*/
        public IEnumerable<EnrollStudentInCourse> GetEnrollCourses
        {
            get
            {
                try
                {
                    Query = "SELECT * FROM t_StudentEnrollInCourse";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<EnrollStudentInCourse> enrollStudentInCourses = new List<EnrollStudentInCourse>();
                    while (DataReader.Read())
                    {
                        EnrollStudentInCourse enrollStudentInCourse = new EnrollStudentInCourse
                        {
                            Id = Convert.ToInt32(DataReader["Id"].ToString()),
                            StudentId = Convert.ToInt32(DataReader["StudentId"].ToString()),
                            CourseId = Convert.ToInt32(DataReader["CourseId"].ToString()),
                            EnrollDate = Convert.ToDateTime(DataReader["EnrollDate"].ToString()),
                            Status = Convert.ToBoolean(DataReader["IsStudentActive"].ToString())

                        };
                        enrollStudentInCourses.Add(enrollStudentInCourse);
                    }
                    DataReader.Close();
                    return enrollStudentInCourses;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect enrolled courses", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }
            }
        }
        /*Get All Student Result*/
        public IEnumerable<StudentResult> GetAllResult
        {
            get
            {
                try
                {
                    Query = "SELECT * FROM t_StudentResult";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<StudentResult> studentResults = new List<StudentResult>();
                    while (DataReader.Read())
                    {
                        StudentResult studentResult = new StudentResult
                        {
                            Id = Convert.ToInt32(DataReader["Id"].ToString()),
                            CourseId = Convert.ToInt32(DataReader["CourseId"].ToString()),
                            StudentId = Convert.ToInt32(DataReader["StudentId"].ToString()),
                            Grade = DataReader["Grade"].ToString(),
                            Status = (bool)DataReader["IsStudentActive"]
                        };
                        studentResults.Add(studentResult);
                    }
                    DataReader.Close();
                    return studentResults;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect student result", exception);
                }
                finally
                {
                    Command.Dispose();
                    Connection.Close();
                }
            }
        }
        /*Get Student Result By Student Id*/
        public IEnumerable<StudentResultViewModel> GetStudentResultByStudentId(int id)
        {
            try
            {
                Command = new SqlCommand("Pro_GetStudentResult", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@studentId", id);
                Connection.Open();
                DataReader = Command.ExecuteReader();
                List<StudentResultViewModel> studentResults = new List<StudentResultViewModel>();
                while (DataReader.Read())
                {
                    StudentResultViewModel studentResult = new StudentResultViewModel
                    {
                        StudentId = Convert.ToInt32(DataReader["StudentId"].ToString()),
                        Code = DataReader["Code"].ToString(),
                        Name = DataReader["Name"].ToString(),
                        Grade = DataReader["Grade"].ToString()
                    };
                    studentResults.Add(studentResult);
                }
                DataReader.Close();
                return studentResults;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to collect sudent result", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }

        /*Insert Student Infromation*/
        public int Insert(Student aStudent)
        {
            try
            {
                Query = "INSERT INTO t_Student ([RegNo],[Name],[Email],[ContactNo],[RegisterationDate],[Address],[DepartmentId]) VALUES(@RegNo,@Name,@Email,@ContactNo,@RegisterationDate,@Address,@DepartmentId)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();

                Command.Parameters.AddWithValue("@RegNo", aStudent.RegNo);
                Command.Parameters.AddWithValue("@Name", aStudent.Name);
                Command.Parameters.AddWithValue("@Email", aStudent.Email.ToLower());
                Command.Parameters.AddWithValue("@ContactNo", aStudent.Contact);
                Command.Parameters.AddWithValue("@RegisterationDate", aStudent.RegDate.ToShortDateString());
                Command.Parameters.AddWithValue("@Address", aStudent.Address);
                Command.Parameters.AddWithValue("@DepartmentId", aStudent.DepartmentId);
                Connection.Open();
                int rowAffected = Command.ExecuteNonQuery();
                return rowAffected;
            }
            catch (Exception exception)
            {
                throw new Exception("Could Not save", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }
        /*Insert Student Course Enroll Infromation*/
        public int Insert(EnrollStudentInCourse enrollStudent)
        {
            try
            {
                Query = "INSERT INTO t_StudentEnrollInCourse(StudentId,CourseId,EnrollDate,IsStudentActive) VALUES(@stId,@courseId,@enrollDate,@status)";
                Command = new SqlCommand(Query, Connection);
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@stId", enrollStudent.StudentId);
                Command.Parameters.AddWithValue("@courseId", enrollStudent.CourseId);

                Command.Parameters.AddWithValue("@enrollDate", enrollStudent.EnrollDate.ToShortDateString());
                Command.Parameters.AddWithValue("@status", 1);
                Connection.Open();
                int rowAffected = Command.ExecuteNonQuery();
                return rowAffected;
            }
            catch (Exception exception)
            {
                throw new Exception("Could not Save Student Course Enroll ", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }

        }

        /*Insert Student Result Infromation*/
        public int Insert(StudentResult studentResult)
        {
            try
            {
                Query = "INSERT INTO t_StudentResult ([StudentId],[CourseId],[Grade],[IsStudentActive]) VALUES(@stId,@courseId,@grade,@isStudentActive)";
                Command = new SqlCommand(Query, Connection); 

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@stId", studentResult.StudentId);
                Command.Parameters.AddWithValue("@courseId", studentResult.CourseId);
                Command.Parameters.AddWithValue("@grade", studentResult.Grade);
                Command.Parameters.AddWithValue("@isStudentActive", 1);
                Connection.Open();
                int rowAffected = Command.ExecuteNonQuery();
                return rowAffected;
            }
            catch (Exception exception)
            {
                throw new Exception("Could not save", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }

        public int Update(EnrollStudentInCourse enrollStudentInCourse)
        {
            Connection.Open();
            SqlTransaction sqlTransaction = Connection.BeginTransaction();
            try
            {
                Command.Transaction = sqlTransaction;
                Query = "UPDATE t_StudentEnrollInCourse SET IsStudentActive=1 WHERE StudentId='" + enrollStudentInCourse.StudentId + "' AND CourseId='" + enrollStudentInCourse.CourseId + "'";
                Command = new SqlCommand(Query, Connection);
                int updateResult = Command.ExecuteNonQuery();

                sqlTransaction.Commit();
                return updateResult;
            }
            catch (Exception exception)
            {
                sqlTransaction.Rollback();
                throw new Exception("Could not Save Enroll Student InCourse", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }

        public int UpdateStudentResult(StudentResult studentResult)
        {
            Query = "UPDATE t_StudentResult SET IsStudentActive=1,Grade='" + studentResult.Grade + "' WHERE StudentId='" +
                                     studentResult.StudentId + "' AND CourseId='" + studentResult.CourseId + "'";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            int i = Command.ExecuteNonQuery();
            Connection.Close();
            return i;
        }
    }
}