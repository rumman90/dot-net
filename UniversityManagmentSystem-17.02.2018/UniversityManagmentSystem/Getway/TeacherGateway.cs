﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class TeacherGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
 
        /*Teache Information Insert Start*/
        public int Insert(Teacher teacher)
        {
            try
            {
                Command = new SqlCommand("Pro_TeacherInsertUpdate", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Name", teacher.Name);
                Command.Parameters.AddWithValue("@Address", teacher.Address);
                Command.Parameters.AddWithValue("@Email", teacher.Email.ToLower());
                Command.Parameters.AddWithValue("@Contact", teacher.Contact);
                Command.Parameters.AddWithValue("@DesignationId", teacher.DesignationId);
                Command.Parameters.AddWithValue("@DepartmentId", teacher.DepartmentId);
                Command.Parameters.AddWithValue("@CreditTobeTaken", teacher.CreditTobeTaken);
                Command.Parameters.AddWithValue("@RemainingCredit", 0);
                Connection.Open();
                return Command.ExecuteNonQuery();

            }
            catch (Exception exception)
            {
                throw new Exception("Could Not Save Teacher Information", exception);
            }

            finally
            {
                Connection.Close();
            }
        }
        /*Teache Information Insert Start*/
        /*Get Teache Information Start*/

        public IEnumerable<Teacher> GetAll()
        {
            try
            {
                Query = "SELECT * FROM t_Teacher";
                Command = new SqlCommand(Query, Connection);
                Connection.Open();
                DataReader = Command.ExecuteReader();
                List<Teacher> teacherList = new List<Teacher>();
                while (DataReader.Read())
                {
                    Teacher teacher = new Teacher
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Address = DataReader["Address"].ToString(),
                        Email = DataReader["Email"].ToString(),
                        Contact = DataReader["Contact"].ToString(),
                        DesignationId = Convert.ToInt32(DataReader["DesignationId"].ToString()),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        CreditTobeTaken = Convert.ToDouble(DataReader["CreditToBeTaken"].ToString()),
                        CreditTaken = Convert.ToDouble(DataReader["RemainingCredit"].ToString())
                    };
                    teacherList.Add(teacher);
                }
                DataReader.Close();
                return teacherList;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Read Teachers Information", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }
        /*Get Teache Information By Email*/
        public Teacher GetTeacherByEmailAddress(string email)
        {
            try
            {
                Query = "SELECT * FROM t_Teacher WHERE Email=@email";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@email", email);
                Teacher teacher = null;
                Connection.Open();
                DataReader = Command.ExecuteReader();
                if (DataReader.Read())
                {
                    teacher = new Teacher
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Address = DataReader["Address"].ToString(),
                        Email = DataReader["Email"].ToString(),
                        Contact = DataReader["Contact"].ToString(),
                        DesignationId = Convert.ToInt32(DataReader["DesignationId"].ToString()),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        CreditTobeTaken = Convert.ToDouble(DataReader["CreditToBeTaken"].ToString())

                    };

                }
                DataReader.Close();
                return teacher;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Read Teachers Information By Email", exception);
            }
            finally
            {
                Connection.Close();
            }
        }
        /*Get Teache Information End*/
        public int UpdateTeacherInformation()
        {
            Query = "UPDATE t_Teacher SET RemainingCredit=0";
            Command = new SqlCommand(Query, Connection);

            Connection.Open();
            int i = Command.ExecuteNonQuery();
            Connection.Close();
            return i;
        }
    }
}