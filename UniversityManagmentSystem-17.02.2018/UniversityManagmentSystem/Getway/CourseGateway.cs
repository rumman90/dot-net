﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class CourseGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
        TeacherGateway _teacherGateway = new TeacherGateway();
        
/*Course Insert Into DataBase Start*/
        public int Insert(Course aCourse)
        {
            try
            {
                Command = new SqlCommand("Pro_CourseInsertUpdate", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Code", aCourse.Code.ToUpper());
                Command.Parameters.AddWithValue("@Name", aCourse.Name);
                Command.Parameters.AddWithValue("@Credit", aCourse.Credit);
                Command.Parameters.AddWithValue("@Description", aCourse.Description);
                Command.Parameters.AddWithValue("@DepartmentId", aCourse.DepartmentId);
                Command.Parameters.AddWithValue("@SemesterId", aCourse.SemesterId);
                Connection.Open();
                return Command.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Seve course", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }

        }
/*Course Insert Into DataBase End*/
/*Get Course Infromation into DataBase Start*/
        public IEnumerable<Course> GetAll
        {
            get
            {
                try
                {
                    Query = "SELECT * FROM t_Course";
                    Command = new SqlCommand(Query, Connection);

                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<Course> courses = new List<Course>();
                    while (DataReader.Read())
                    {
                        Course course = new Course
                        {
                            Id = Convert.ToInt32(DataReader["Id"].ToString()),
                            Name = DataReader["Name"].ToString(),
                            Code = DataReader["Code"].ToString(),
                            Credit = Convert.ToDouble(DataReader["Credit"].ToString()),
                            Description = DataReader["Descirption"].ToString(),
                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                            SemesterId = Convert.ToInt32(DataReader["SemesterId"].ToString())

                        };
                        courses.Add(course);
                    }
                    DataReader.Close();
                    return courses;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to Read Courses Data", exception);
                }
                finally
                {
                    Connection.Close();
                }
            }
        }
        /*Get Course Infromation By Name Start*/
        public Course GetCourseByName(string name)
        {
            try
            {
                Query = "SELECT * FROM t_Course WHERE Name=@name";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@name", name);
                Connection.Open();
                Course course = null;
                DataReader = Command.ExecuteReader();
                if (DataReader.Read())
                {
                    course = new Course
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Code = DataReader["Code"].ToString(),
                        Credit = Convert.ToDouble(DataReader["Credit"].ToString()),
                        Description = DataReader["Descirption"].ToString(),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        SemesterId = Convert.ToInt32(DataReader["SemesterId"].ToString())

                    };

                }
                DataReader.Close();
                return course;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Collect course By Name", exception);
            }
            finally
            {
                Connection.Close();
            }
        }
        /*Get Course Infromation By Name End*/
        /*Get Course Infromation By Code Start*/
        public Course GetCourseByCode(string code)
        {
            try
            {
                Query = "SELECT * FROM t_Course WHERE Code=@code";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@code", code);
                Connection.Open();
                Course course = null;
                DataReader= Command.ExecuteReader();
                if (DataReader.Read())
                {
                    course = new Course
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Code = DataReader["Code"].ToString(),
                        Credit = Convert.ToDouble(DataReader["Credit"].ToString()),
                        Description = DataReader["Descirption"].ToString(),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        SemesterId = Convert.ToInt32(DataReader["SemesterId"].ToString())

                    };

                }
                DataReader.Close();
                return course;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Collect Course By Code", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }
        /*Get Course Infromation By Code End*/
        /*Get Course Infromation By Student ID Start*/
        public IEnumerable<Course> GetCoursesTakeByaStudentByStudentId(int id)
        {
            try
            {
                Command = new SqlCommand("Pro_GetCoursesTakenByaStudent", Connection);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@StudentId", id);
                Connection.Open();
                DataReader= Command.ExecuteReader();
                List<Course> courses = new List<Course>();
                while (DataReader.Read())
                {
                    Course aCourse = new Course
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Code = DataReader["Code"].ToString(),
                        Credit = Convert.ToDouble(DataReader["Credit"].ToString()),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        Description = DataReader["Descirption"].ToString(),
                        SemesterId = Convert.ToInt32(DataReader["SemesterId"].ToString())
                    };
                    courses.Add(aCourse);
                }
                DataReader.Close();
                return courses;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Collect Courses", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }
        /*Get Course Infromation By Student ID End*/
        /*Get Course Infromation By Department ID Start*/
        public IEnumerable<Course> GetCourseByDepartmentId(int departmentId)
        {

            try
            {
                Query = "SELECT * FROM t_Course WHERE DepartmentId='" + departmentId + "'";
                Command= new SqlCommand(Query, Connection);

                List<Course> courses = new List<Course>();
                Connection.Open();
                DataReader= Command.ExecuteReader();

                while (DataReader.Read())
                {
                    Course course = new Course
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Code = DataReader["Code"].ToString(),
                        Credit = Convert.ToDouble(DataReader["Credit"].ToString()),
                        Description = DataReader["Descirption"].ToString(),
                        DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                        SemesterId = Convert.ToInt32(DataReader["SemesterId"].ToString())

                    };
                    courses.Add(course);
                }
                DataReader.Close();
                return courses;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to collect Courses by Department Id", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }
/*Get Course Infromation into DataBase End*/
        public IEnumerable<CourseViewModel> GetCourseViewModels
        {
            get
            {
                try
                {
                    Command = new SqlCommand("Pro_GetCourseInformation", Connection);
                    Command.CommandType = CommandType.StoredProcedure;
                   
                    Connection.Open();
                    DataReader= Command.ExecuteReader();
                    List<CourseViewModel> courseViewModels = new List<CourseViewModel>();
                    while (DataReader.Read())
                    {
                        CourseViewModel course = new CourseViewModel
                        {
                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                            Name = DataReader["Name"].ToString(),
                            Code = DataReader["Code"].ToString(),
                            Semester = DataReader["Semester"].ToString(),
                            Teacher = DataReader["Teacher"].ToString()
                        };
                        courseViewModels.Add(course);
                    }
                    DataReader.Close();
                    return courseViewModels;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect Course statics", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }
            }
        }

/*Un Assign Course Start*/    
        public int UnAssignCourse()
        {
            Connection.Open();
            SqlTransaction sqlTransaction = Connection.BeginTransaction();
            try
            {
                Query = "UPDATE t_CourseAssignToTeacher SET IsActive=0";
                Command = new SqlCommand(Query, Connection);
                Command.Transaction = sqlTransaction;
                Command.ExecuteNonQuery();
                _teacherGateway.UpdateTeacherInformation();
                int a = ResetStudentResult();
                int i = UnAssignStudentCourse();
                sqlTransaction.Commit();
                return i;
            }
            catch (Exception exception)
            {
                sqlTransaction.Rollback();
                throw new Exception("Failed to UnAssign course", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }

        private int UnAssignStudentCourse()
        {
            Command.CommandText = "UPDATE t_StudentEnrollInCourse SET IsStudentActive=0";
            return Command.ExecuteNonQuery();
        }

        private int ResetStudentResult()
        {
            Command.CommandText = "UPDATE t_StudentResult SET IsStudentActive=0";
            return Command.ExecuteNonQuery();
        }
/*Un Assign Course End*/   
 
    }
}