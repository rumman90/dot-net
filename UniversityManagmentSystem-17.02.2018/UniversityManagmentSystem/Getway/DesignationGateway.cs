﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class DesignationGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
        public IEnumerable<Designation> GetAll
        {
            get
            {
                try
                {
                    List<Designation> desingantionlist = new List<Designation>();
                    string query = "SELECT * FROM t_Designation";
                    SqlCommand command = new SqlCommand(query, Connection);
                    command.CommandText = query;
                    Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        Designation designation = new Designation
                        {
                            Id = Convert.ToInt32(reader["Id"].ToString()),
                            Designations = reader["Designations"].ToString()
                        };
                        desingantionlist.Add(designation);
                    }

                    reader.Close();

                    return desingantionlist;
                }
                catch (Exception exception)
                {
                    throw new Exception("Could not collect Designations", exception);
                }
                finally
                {
                    Connection.Close();
                }
            }
        }
    }
}