﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class DayGateway:DBGetway
    {private SqlConnection Connection = ConnectGlobal();
        public IEnumerable<Day> GetAllDays
        {
            get
            {
                try
                {
                    var dayList = new List<Day>();
                    Query = "SELECT * FROM t_Day";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    SqlDataReader reader = Command.ExecuteReader();

                    while (reader.Read())
                    {
                        var day = new Day
                        {
                            Id = Convert.ToInt32(reader["Id"].ToString()),
                            Name = reader["Name"].ToString()
                        };
                        dayList.Add(day);
                    }
                    reader.Close();
                    return dayList;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to collect days", exception);
                }
                finally
                {
                    Connection.Close();
                    Command.Dispose();
                }
            }
        }
    }
}