﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class SemesterGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
        public List<Semester> GetAll
        {
            get
            {
                try
                {
                    string query = "SELECT * FROM t_Semester";
                    SqlCommand command = new SqlCommand(query, Connection);
                    command.CommandText = query;
                    List<Semester> semesters = new List<Semester>();
                    Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Semester semester = new Semester
                        {
                            Id = Convert.ToInt32(reader["Id"].ToString()),
                            Name = reader["Name"].ToString(),

                        };
                        semesters.Add(semester);
                    }
                    reader.Close();
                    return semesters;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to connect semster", exception);
                }
                finally
                {
                    Connection.Close();
                }
            }
        }
    }
}