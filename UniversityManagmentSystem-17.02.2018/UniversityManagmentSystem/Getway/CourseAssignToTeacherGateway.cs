﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagmentSystem.Manager;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class CourseAssignToTeacherGateway:DBGetway
    {
        private SqlConnection Connection = ConnectGlobal();
        TeacherManager _teacherManager = new TeacherManager();
        public int Insert(CourseAssignToTeacher courseAssign)
        {
            Connection.Open();
            SqlTransaction sqlTransaction = Connection.BeginTransaction();
            try
            {
                Command.Transaction = sqlTransaction;
                Command.CommandText = "INSERT INTO t_CourseAssignToTeacher(DepartmentId,TeacherId,CourseId,IsActive) VALUES(@deptId,@teacherId,@courseId,@status)";
                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@deptId", courseAssign.DepartmentId);
                Command.Parameters.AddWithValue("@teacherId", courseAssign.TeacherId);
                Command.Parameters.AddWithValue("@courseId", courseAssign.CourseId);
                Command.Parameters.AddWithValue("@status", 1);
                int rowAffected = Command.ExecuteNonQuery();

                int updateResult = UpdateTeacher(courseAssign);
                sqlTransaction.Commit();
                return rowAffected;
            }
            catch (Exception exception)
            {
                sqlTransaction.Rollback();
                throw new Exception("Could not Course Assign Information Save", exception);
            }
            finally
            {
                Connection.Close();
                Command.Dispose();
            }
        }
        private int UpdateTeacher(CourseAssignToTeacher courseAssign)
        {
            Teacher teacher = _teacherManager.GetAll().ToList().Find(t => t.Id == courseAssign.TeacherId);

            double creditTakenbyTeacher = Convert.ToDouble(teacher.CreditTaken) + Convert.ToDouble(courseAssign.Credit);
            Command.CommandText = "Update t_Teacher Set RemainingCredit='" + creditTakenbyTeacher + "' WHERE Id='" +
                                     courseAssign.TeacherId + "'";
            return Command.ExecuteNonQuery();
        }
        public IEnumerable<CourseAssignToTeacher> GetAll
        {
            get
            {
                try
                {
                    Query = "SELECT * FROM t_CourseAssignToTeacher";
                    Command = new SqlCommand(Query, Connection);
                    Connection.Open();
                    DataReader = Command.ExecuteReader();
                    List<CourseAssignToTeacher> courseAssignToTeachers = new List<CourseAssignToTeacher>();
                    while (DataReader.Read())
                    {
                        CourseAssignToTeacher assignToTeacher = new CourseAssignToTeacher
                        {
                            Id = Convert.ToInt32(DataReader["Id"].ToString()),
                            DepartmentId = Convert.ToInt32(DataReader["DepartmentId"].ToString()),
                            TeacherId = Convert.ToInt32(DataReader["TeacherId"].ToString()),
                            CourseId = Convert.ToInt32(DataReader["CourseId"].ToString()),
                            Status = Convert.ToBoolean(DataReader["IsActive"].ToString())

                        };
                        courseAssignToTeachers.Add(assignToTeacher);
                    }
                    DataReader.Close();
                    return courseAssignToTeachers;
                }
                catch (Exception exception)
                {
                    throw new Exception("Unable to Read Course Assign Data", exception);
                }
                finally
                {
                    Command.Dispose();
                    Connection.Close();
                }
            }
        }

        public int Update(CourseAssignToTeacher courseAssign)
        {
            Connection.Open();
            SqlTransaction sqlTransaction = Connection.BeginTransaction();
            try
            {


                Command.Transaction = sqlTransaction;
                Command.CommandText = "UPDATE t_CourseAssignToTeacher SET IsActive=1 WHERE TeacherId='" + courseAssign.TeacherId + "' AND CourseId='" + courseAssign.CourseId + "'";
                Command.ExecuteNonQuery();

                int updateResult = UpdateTeacher(courseAssign);
                sqlTransaction.Commit();
                return updateResult;
            }
            catch (Exception exception)
            {
                sqlTransaction.Rollback();
                throw new Exception("Could not Teacher Course Assign!!", exception);
            }
            finally
            {
                Command.Dispose();
                Connection.Close();
            }
        }
    }
}