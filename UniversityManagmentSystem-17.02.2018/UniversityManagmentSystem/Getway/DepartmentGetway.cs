﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using UniversityManagmentSystem.Models;

namespace UniversityManagmentSystem.Getway
{
    public class DepartmentGetway:DBGetway
    {
       private SqlConnection Connection = ConnectGlobal();

       public Department GetDepartmentByCode(string code)
       {
           try
           {
               Query = "SELECT * FROM t_Departments WHERE Code=@code";
               Command = new SqlCommand(Query, Connection);

               Command.Parameters.Clear();
               Command.Parameters.AddWithValue("@code", code);
               Department department = null;
               Connection.Open();
               DataReader = Command.ExecuteReader();
               if (DataReader.Read())
               {
                   department = new Department
                   {
                       Id = Convert.ToInt32(DataReader["Id"].ToString()),
                       Name = DataReader["Name"].ToString(),
                       Code = DataReader["Code"].ToString()
                   };

               }
               DataReader.Close();
               return department;
           }
           catch (Exception exception)
           {
               throw new Exception("Unable to connect department", exception);
           }
           finally
           {
               Connection.Close();
           }
       }

       public Department GetDepartmentByName(string name)
       {
           try
           {
               string query = "SELECT * FROM t_Departments WHERE Name=@name";
               SqlCommand command = new SqlCommand(query, Connection);
               command.CommandText = query;
               command.Parameters.Clear();
               command.Parameters.AddWithValue("@name", name);
               Department department = null;
               Connection.Open();
               SqlDataReader reader = command.ExecuteReader();
               if (reader.Read())
               {
                   department = new Department
                   {
                       Id = Convert.ToInt32(reader["Id"].ToString()),
                       Name = reader["Name"].ToString(),
                       Code = reader["Code"].ToString()
                   };

               }
               reader.Close();
               return department;
           }
           catch (Exception exception)
           {
               throw new Exception("Unable to connect department", exception);
           }
           finally
           {
               Connection.Close();
           }
       }
        public int Insert(Department aDepartment)
        {
            try
            {
                Query = "INSERT INTO t_Departments(Code, Name) VALUES(@Code, @Name)";
                Command = new SqlCommand(Query, Connection);

                Command.Parameters.Clear();
                Command.Parameters.AddWithValue("@Code", aDepartment.Code.ToUpper());
                Command.Parameters.AddWithValue("@Name", aDepartment.Name);
                Connection.Open();
                return Command.ExecuteNonQuery();
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to Department Save", exception);
            }
            finally
            {
                Connection.Close();
            }
        }

        public IEnumerable<Department> GetAll()
        {
            try
            {
                Query = "SELECT * FROM t_Departments";
                Command = new SqlCommand(Query, Connection);

                List<Department> departments = new List<Department>();
                Connection.Open();
                DataReader = Command.ExecuteReader();
                while (DataReader.Read())
                {
                    Department department = new Department
                    {
                        Id = Convert.ToInt32(DataReader["Id"].ToString()),
                        Name = DataReader["Name"].ToString(),
                        Code = DataReader["Code"].ToString()
                    };

                    departments.Add(department);
                }
                DataReader.Close();
                return departments;
            }
            catch (Exception exception)
            {
                throw new Exception("Unable to connect department", exception);
            }
            finally
            {
                Connection.Close();
            }
        }


    }
}